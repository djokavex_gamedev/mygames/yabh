#extends KinematicBody2D
extends RigidBody2D

export (float) var ACCELERATION = 500.0
#export (float) var ACCELERATION = 200.0
export (float) var MAX_SPEED = 100.0
export (float) var player_id = 1

var health = Global.player_lives setget set_health

var perfect_shield = false
var start_time_shield = 0

var is_bigger = false
var start_time_bigger = 0
var is_slower = false
var start_time_slower = 0
var is_invisible = false
var start_time_invisible = 0

signal health_changed
signal died
signal power_down_enemy

var vel = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("player")

	#var text_gui = get_tree().get_root().find_node("Player2",true,false)
	#self.connect("power_down_enemy",text_gui,"on_player_powerdown")
	
	health = Global.player_lives
	
	if health <= 2:
		$Shield2.visible = false
	if health <= 1:
		$Shield1.visible = false
	
	$PlayerGui/ShieldTimer.visible = false
	$PerfectShieldSprite.visible = false
	
	$PlayerGui/player_id.text = "player" + str(player_id)
	#add_force(Vector2(), Vector2(0, 100))
	
	if Global.mode == Global.modes.online_vs:
		collision_mask = 2 # Do not collide with other player in online
	else:
		collision_mask = 1
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
func _integrate_forces(state):
	vel = Vector2(0,0) # TO REMOVE
	
	if Global.mode == Global.modes.online_vs:
		if(is_network_master()):
			
			vel = computeMovePlayer()
			set_applied_force(state.get_total_gravity() + vel)
			
			var time_now = OS.get_ticks_msec()
		
			computePerfectShield(time_now)
			
			if position.x >= get_viewport_rect().size.x:
				set_health(0)
			if position.x <= 0:
				set_health(0)
			if position.y >= get_viewport_rect().size.y:
				set_health(0)
			if position.y <= 0:
				set_health(0)
				
			computeDebuff(time_now)
			
			# Tell the other computer about our new position so it can update       
			rpc_unreliable("setPosition",position, rotation)
			rpc_unreliable("setShield", perfect_shield)
			rpc_unreliable("setDebuff", is_bigger, is_slower, is_invisible)
			
			#rpc("setPosition",Vector2(position.x, position.y), rotation)
			#rpc("setShield", perfect_shield)
			#rpc("setDebuff", is_bigger, is_slower, is_invisible)
  
			pass
		pass
	else:
			
		vel = computeMovePlayer()
		set_applied_force(state.get_total_gravity() + vel)
		
		var time_now = OS.get_ticks_msec()
		
		computePerfectShield(time_now)
		
		
	#	vel.x = 0.0
	#	vel.y = 0.0
	#	if Input.is_action_pressed("ui_right"):
	#		vel.x = MAX_SPEED
	#	if Input.is_action_pressed("ui_left"):
	#		vel.x = -MAX_SPEED
	#	if Input.is_action_pressed("ui_up"):
	#		vel.y = -MAX_SPEED
	#	if Input.is_action_pressed("ui_down"):
	#		vel.y = MAX_SPEED
	#
	#	var velocity = Vector2()
	#	velocity = Vector2(vel.x, vel.y).rotated(rotation)
		
		#move_and_slide(vel)
		#move_and_collide(vel)
		
		
		if position.x >= get_viewport_rect().size.x:
			set_health(0)
		if position.x <= 0:
			set_health(0)
		if position.y >= get_viewport_rect().size.y:
			set_health(0)
		if position.y <= 0:
			set_health(0)
			
		#is_colliding()
	#	$Sprite.flip_h = true

		computeDebuff(time_now)
			
		
	$PlayerGui/player_id.text = "player" + str(player_id)
	$PlayerGui/life.text = "Life: " + str(health)
	
	pass

func computeMovePlayer() -> Vector2:
	var delta = 1
	var vel = Vector2(0,0)
	if Global.mode != Global.modes.versus:
		look_at(get_global_mouse_position())
	
	
	if (player_id == 1) or (Global.mode == Global.modes.online_vs):
		if Input.is_action_pressed("ui_right"):
			vel.x += ACCELERATION * delta
		if Input.is_action_pressed("ui_left"):
			vel.x += -ACCELERATION * delta
		if Input.is_action_pressed("ui_up"):
			vel.y += -ACCELERATION * delta
		if Input.is_action_pressed("ui_down"):
			vel.y += ACCELERATION * delta
	
	if player_id == 2 and (Global.mode != Global.modes.online_vs):
		if Input.is_action_pressed("ui_right2"):
			vel.x += ACCELERATION * delta
		if Input.is_action_pressed("ui_left2"):
			vel.x += -ACCELERATION * delta
		if Input.is_action_pressed("ui_up2"):
			vel.y += -ACCELERATION * delta
		if Input.is_action_pressed("ui_down2"):
			vel.y += ACCELERATION * delta
			
	if vel.x > MAX_SPEED:
		vel.x = MAX_SPEED
	if vel.x < -MAX_SPEED:
		vel.x = -MAX_SPEED
	if vel.y < -MAX_SPEED:
		vel.y = -MAX_SPEED
	if vel.y > MAX_SPEED:
		vel.y = MAX_SPEED
		
	return vel

func computePerfectShield(current_time):
	var elapsed = (current_time - start_time_shield) / 1000.0
	
	if (player_id == 1) or (Global.mode == Global.modes.online_vs):
		if elapsed > 5.0 && Input.is_action_just_pressed("ui_select"):
			perfect_shield = true
			start_time_shield = OS.get_ticks_msec()
			$PerfectShieldSprite.visible = true
	if (player_id == 2) and (Global.mode != Global.modes.online_vs):
		if elapsed > 5.0 && Input.is_action_just_pressed("ui_select2"):
			perfect_shield = true
			start_time_shield = OS.get_ticks_msec()
			$PerfectShieldSprite.visible = true
		
	elapsed = (current_time - start_time_shield) / 1000.0	
	#print(elapsed)
	if perfect_shield && (elapsed) > Global.player_perfect_shield_time:
		#print(elapsed)
		perfect_shield = false
		$PerfectShieldSprite.visible = false
		#print("perfect stop")
		#print(time_now)
	
	if max(0, 5.0 - (int(elapsed))) > 0:
		$PlayerGui/ShieldTimer.visible = true
		$PlayerGui/ShieldTimer.text = str(max(0, 5.0 - (int(elapsed))) )
	else:
		$PlayerGui/ShieldTimer.visible = false

func computeDebuff(current_time):
	var elapsed = 0
	if is_bigger:
		elapsed = (current_time - start_time_bigger) / 1000.0
		if (elapsed) > Global.power_down_bigger_time:
			scale.x = scale.x - Global.power_down_bigger_size
			scale.y = scale.y - Global.power_down_bigger_size
			start_time_bigger = 0
			is_bigger = false
		pass
	if is_slower:
		elapsed = (current_time - start_time_slower) / 1000.0
		if (elapsed) > Global.power_down_slower_time:
			MAX_SPEED = MAX_SPEED + Global.power_down_slower_value
			start_time_slower = 0
			is_slower = false
		pass
	if is_invisible:
		elapsed = (current_time - start_time_invisible) / 1000.0
		if (elapsed) > Global.power_down_invisible_time:
			self.visible = true
			start_time_invisible = 0
			is_invisible = false
		pass
			

#func _physics_process(delta):
#	pass
	
func set_health(value):
	#var hit_sound = AudioStreamPlayer.new()
	#self.add_child(hit_sound)
	#hit_sound.stream = load('res://sound/sfx_shieldDown.ogg')
	#hit_sound.play()
	
	health = value
	emit_signal('health_changed', health)
	if health == 2:
		$Shield2.visible = false
	if health == 1:
		$Shield1.visible = false
	if health <= 0:
		emit_signal('died')
		Global.looser = player_id
		get_tree().change_scene("res://scene/GameOver.tscn")
		queue_free()
	pass
	
func on_player_powerdown(power_id):
	#print("Player 1 receive power down")
	if power_id == Global.powerdown.bigger:
		if start_time_bigger == 0:
			scale.x = scale.x + Global.power_down_bigger_size
			scale.y = scale.y + Global.power_down_bigger_size
		start_time_bigger = OS.get_ticks_msec()
		is_bigger = true
	if power_id == Global.powerdown.slower:
		if start_time_slower == 0:
			MAX_SPEED = MAX_SPEED - Global.power_down_slower_value
		start_time_slower = OS.get_ticks_msec()
		is_slower = true
	if power_id == Global.powerdown.invisible:
		if start_time_invisible == 0:
			self.visible = false
		start_time_invisible = OS.get_ticks_msec()
		is_invisible = true
	pass
	
func take_power_down(power_id):
	emit_signal('power_down_enemy', power_id)
	pass


##################################
##################################
# ONLINE #
##################################
##################################


slave func setPosition(pos, rot):
	position = pos
	rotation = rot
	
slave func setShield(shield):
	$PerfectShieldSprite.visible = shield
	
slave func setDebuff(bigger, slower, invisible):
	self.visible = not invisible
  
master func shutItDown():
  #Send a shutdown command to all connected clients, including this one
  rpc("shutDown")
  
sync func shutDown():
  get_tree().quit()

