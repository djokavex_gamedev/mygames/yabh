extends Button

export(String) var action = "ui_up"

func _ready():
	#add_to_group("persistant_option")
	assert(InputMap.has_action(action))
	#set_process_unhandled_key_input(false)
	set_process_input(false)
	display_current_key()
	#print(action + " end ready")


func _toggled(button_pressed):
	#set_process_unhandled_key_input(button_pressed)
	set_process_input(button_pressed)
	if button_pressed:
		text = "Press a key"
		release_focus()
	else:
		display_current_key()

#func _unhandled_key_input(event):
func _input(event):
	# Note that you can use the _input callback instead, especially if
	# you want to work with gamepads.
	#print("input "+event.as_text())
	if event is InputEventKey:
		remap_action_to(event)
		pressed = false
	
	if event is InputEventMouseButton:
		print("Mouse button pressed "+event.as_text())
		var myevent = InputEventMouseButton.new()
		#InputMap.add_action(value)
		myevent.button_index = event.button_index
		#InputMap.action_add_event(button, event)
		remap_action_to(myevent)
		pressed = false
		pass
		
	if event is InputEventJoypadButton:
		print("joy button pressed "+event.as_text())
		remap_action_to(event)
		pressed = false


func remap_action_to(event):
	InputMap.action_erase_events(action)
	InputMap.action_add_event(action, event)
	#print(InputMap.action_has_event(action, event))
	#text = "%s" % event.as_text()
	print("Action " + action + " event " + event.as_text())
	display_current_key()


func display_current_key():
	var current_key = InputMap.get_action_list(action)[0].as_text()
	#text = "%s" % current_key
	#print(action + " " + text)
	if InputMap.get_action_list(action)[0] is InputEventKey:
		text = "%s" % current_key
		return InputMap.get_action_list(action)[0].as_text()
	if InputMap.get_action_list(action)[0] is InputEventMouseButton:
		text = "%d" % InputMap.get_action_list(action)[0].button_index
		return InputMap.get_action_list(action)[0].button_index
	if InputMap.get_action_list(action)[0] is InputEventJoypadButton:
		text = "%d" % InputMap.get_action_list(action)[0].button_index
		return InputMap.get_action_list(action)[0].button_index
		

func display_current_key_type():
	if InputMap.get_action_list(action)[0] is InputEventKey:
		return "InputEventKey"
	if InputMap.get_action_list(action)[0] is InputEventMouseButton:
		return "InputEventMouseButton"
	if InputMap.get_action_list(action)[0] is InputEventJoypadButton:
		return "InputEventJoypadButton"



