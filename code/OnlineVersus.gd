extends Node2D

var rand = RandomNumberGenerator.new()
var enemy_1 = preload("res://scene/Enemy.tscn")

var min_enemy_speed = 150
var max_enemy_speed = 250

var MAX_VELOCITY = 500
var MIN_SPAWN_TIME = 0.05

# Called when the node enters the scene tree for the first time.
func _ready():
	#First create ourself
	var thisPlayer = preload("res://scene/Player.tscn").instance()
	thisPlayer.set_name(str(get_tree().get_network_unique_id()))
	thisPlayer.set_network_master(get_tree().get_network_unique_id())
	if get_tree().is_network_server():
		thisPlayer.position = Vector2(200,200)
		thisPlayer.player_id = 1
	else:
		thisPlayer.position = Vector2(400,400)
		thisPlayer.player_id = 2
	add_child(thisPlayer)

	#Now create the other player
	var otherPlayer = preload("res://scene/Player.tscn").instance()
	otherPlayer.set_name(str(Global.network_player))
	otherPlayer.set_network_master(Global.network_player)
	if get_tree().is_network_server():
		otherPlayer.player_id = 2
		pass
	else:
		otherPlayer.player_id = 1
		pass
	add_child(otherPlayer)
	
	thisPlayer.connect("power_down_enemy",otherPlayer,"on_player_powerdown")
	otherPlayer.connect("power_down_enemy",thisPlayer,"on_player_powerdown")
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_EnemySpawner_timeout():
	var enemy = enemy_1.instance()
	#print("velocity" + str(enemy.velocity.x) + str(enemy.velocity.y)) 
	var screen_size = get_viewport().get_visible_rect().size
	
	rand.randomize()
	
	var x = 0
	var y = 0
	var side = rand.randi_range(0, 3)
	rand.randomize()
	if side == 0:
		x = 1
		y = rand.randf_range(1, screen_size.y-1)
		enemy.velocity.x = rand.randf_range(min_enemy_speed, max_enemy_speed)
		enemy.velocity.y = rand.randf_range(-20, 20)
	if side == 1:
		x = rand.randf_range(1, screen_size.x-1)
		y = 1
		enemy.velocity.x = rand.randf_range(-20, 20)
		enemy.velocity.y = rand.randf_range(min_enemy_speed, max_enemy_speed)
	if side == 2:
		x = screen_size.x - 1
		y = rand.randf_range(1, screen_size.y-1)
		enemy.velocity.x = -rand.randf_range(min_enemy_speed, max_enemy_speed)
		enemy.velocity.y = rand.randf_range(-20, 20)
	if side == 3:
		x = rand.randf_range(1, screen_size.x-1)
		y = screen_size.y -1
		enemy.velocity.x = rand.randf_range(-20, 20)
		enemy.velocity.y = -rand.randf_range(min_enemy_speed, max_enemy_speed)

	if enemy.velocity.x > MAX_VELOCITY:
		enemy.velocity.x = MAX_VELOCITY
	if enemy.velocity.x < -MAX_VELOCITY:
		enemy.velocity.x = -MAX_VELOCITY
	if enemy.velocity.y > MAX_VELOCITY:
		enemy.velocity.y = MAX_VELOCITY
	if enemy.velocity.y < -MAX_VELOCITY:
		enemy.velocity.y = -MAX_VELOCITY
	
	enemy.position.x = x
	enemy.position.y = y
	
	enemy.rotation_speed = rand.randf_range(-enemy.rotation_speed, enemy.rotation_speed)
	
	enemy.scale.x = rand.randf_range(0.5, 0.8)
	enemy.scale.y = enemy.scale.x
	
	add_child(enemy)

	
	pass # Replace with function body.
