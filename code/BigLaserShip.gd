extends Area2D

export var velocity = Vector2() 

var laser = preload("res://scene/Laser.tscn")
var target_player = null

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(true)
	add_to_group("enemy")
	connect("body_entered", self, "_on_area_enter")
	
	var players = get_tree().get_nodes_in_group("player")
	randomize()
	if players.size() > 0:
		var ii = randi() % players.size()
		target_player = players[ii]
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	translate(velocity * delta)
	
	#var players = get_tree().get_nodes_in_group("player")
	#var target = null
	#randomize()
	#var ii = randi() % players.size()
	#for p in players:
	#target = players[ii]
		
	if is_instance_valid(target_player):
		$gun.rotation_degrees = rad2deg(target_player.global_position.angle_to_point($gun.global_position))+90-rotation_degrees
	
	pass

func _on_area_enter(other):
	if other.is_in_group("player"):
		other.health -= 5
		queue_free()
	pass


func _on_laserSpawn_timeout():
	var current_laser = laser.instance()
	
	current_laser.position.x = 83.8
	current_laser.position.y = 1.5
	
	current_laser.rotation_degrees = $gun.rotation_degrees
	
	current_laser.velocity = Vector2(100, 0).rotated(deg2rad(current_laser.rotation_degrees-90))
	
	add_child(current_laser)
	
	var players = get_tree().get_nodes_in_group("player")
	randomize()
	var ii = randi() % players.size()
	target_player = players[ii]
	
	pass # Replace with function body.
