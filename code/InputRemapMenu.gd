extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("persistant_option")
	load_option()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func save():
	#print("saving")
	var save_dict = {
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"ui_left_type" : $HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow/RemapButton.display_current_key_type(),
		"ui_left" : $HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow/RemapButton.display_current_key(),
		"ui_right_type" : $HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow2/RemapButton.display_current_key_type(),
		"ui_right" : $HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow2/RemapButton.display_current_key(),
		"ui_up_type" : $HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow3/RemapButton.display_current_key_type(),
		"ui_up" : $HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow3/RemapButton.display_current_key(),
		"ui_down_type" : $HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow4/RemapButton.display_current_key_type(),
		"ui_down" : $HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow4/RemapButton.display_current_key(),
		"ui_shield_type" : $HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow5/RemapButton.display_current_key_type(),
		"ui_shield" : $HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow5/RemapButton.display_current_key(),
		"ui_left2_type" : $HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow/RemapButton.display_current_key_type(),
		"ui_left2" : $HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow/RemapButton.display_current_key(),
		"ui_right2_type" : $HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow2/RemapButton.display_current_key_type(),
		"ui_right2" : $HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow2/RemapButton.display_current_key(),
		"ui_up2_type" : $HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow3/RemapButton.display_current_key_type(),
		"ui_up2" : $HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow3/RemapButton.display_current_key(),
		"ui_down2_type" : $HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow4/RemapButton.display_current_key_type(),
		"ui_down2" : $HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow4/RemapButton.display_current_key(),
		"ui_shield2_type" : $HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow5/RemapButton.display_current_key_type(),
		"ui_shield2" : $HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow5/RemapButton.display_current_key(),
		"end" : 1
	}
	return save_dict

# Note: This can be called from anywhere inside the tree. This function is
# path independent.
# Go through everything in the persist category and ask them to return a
# dict of relevant variables.
func save_option():
	var save_option = File.new()
	save_option.open("user://option.save", File.WRITE)
	var save_nodes = get_tree().get_nodes_in_group("persistant_option")
	for node in save_nodes:
		# Check the node is an instanced scene so it can be instanced again during load.
		if node.filename.empty():
			print("persistent node '%s' is not an instanced scene, skipped" % node.name)
			continue

		# Check the node has a save function.
		if !node.has_method("save"):
			print("persistent node '%s' is missing a save() function, skipped" % node.name)
			continue

		# Call the node's save function.
		var node_data = node.call("save")

		# Store the save dictionary as a new line in the save file.
		save_option.store_line(to_json(node_data))
	save_option.close()

# Note: This can be called from anywhere inside the tree. This function
# is path independent.
func load_option():
	#print("restore")
	var save_option = File.new()
	if not save_option.file_exists("user://option.save"):
		return # Error! We don't have a save to load.

	# We need to revert the game state so we're not cloning objects
	# during loading. This will vary wildly depending on the needs of a
	# project, so take care with this step.
	# For our example, we will accomplish this by deleting saveable objects.
	var save_nodes = get_tree().get_nodes_in_group("persistant_option")
	#for i in save_nodes:
	#	i.queue_free()

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_option.open("user://option.save", File.READ)
	while save_option.get_position() < save_option.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_option.get_line())

		var new_input_key = InputEventKey.new()
		var new_input_mouse = InputEventMouseButton.new()
		var new_input_pad = InputEventJoypadButton.new()
		
		InputMap.action_erase_events("ui_left")
		if node_data["ui_left_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_left"]))
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow/RemapButton.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_left_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_left"])
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow/RemapButton.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_left_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_left"])
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow/RemapButton.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_right")
		if node_data["ui_right_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_right"]))
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow2/RemapButton.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_right_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_right"])
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow2/RemapButton.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_right_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_right"])
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow2/RemapButton.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_up")
		if node_data["ui_up_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_up"]))
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow3/RemapButton.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_up_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_up"])
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow3/RemapButton.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_up_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_up"])
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow3/RemapButton.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_down")
		if node_data["ui_down_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_down"]))
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow4/RemapButton.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_down_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_down"])
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow4/RemapButton.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_down_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_down"])
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow4/RemapButton.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_shield")
		if node_data["ui_shield_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_shield"]))
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow5/RemapButton.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_shield_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_shield"])
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow5/RemapButton.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_shield_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_shield"])
			$HBoxContainer/RemapButtonGroup/ActionsList/ActionRemapRow5/RemapButton.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_left2")
		if node_data["ui_left2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_left2"]))
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow/RemapButton.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_left2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_left2"])
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow/RemapButton.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_left2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_left2"])
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow/RemapButton.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_right2")
		if node_data["ui_right2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_right2"]))
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow2/RemapButton.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_right2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_right2"])
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow2/RemapButton.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_right2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_right2"])
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow2/RemapButton.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_up2")
		if node_data["ui_up2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_up2"]))
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow3/RemapButton.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_up2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_up2"])
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow3/RemapButton.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_up2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_up2"])
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow3/RemapButton.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_down2")
		if node_data["ui_down2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_down2"]))
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow4/RemapButton.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_down2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_down2"])
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow4/RemapButton.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_down2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_down2"])
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow4/RemapButton.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_shield2")
		if node_data["ui_shield2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_shield2"]))
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow5/RemapButton.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_shield2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_shield2"])
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow5/RemapButton.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_shield2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_shield2"])
			$HBoxContainer/RemapButtonGroup2/ActionsList/ActionRemapRow5/RemapButton.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		
	save_option.close()

func load_option2():
	var save_option = File.new()
	if not save_option.file_exists("user://option.save"):
		return # Error! We don't have a save to load.

	# We need to revert the game state so we're not cloning objects
	# during loading. This will vary wildly depending on the needs of a
	# project, so take care with this step.
	# For our example, we will accomplish this by deleting saveable objects.
	#var save_nodes = get_tree().get_nodes_in_group("persistant_option")
	#for i in save_nodes:
	#	i.queue_free()

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_option.open("user://option.save", File.READ)
	while save_option.get_position() < save_option.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_option.get_line())

		var new_input_key = InputEventKey.new()
		var new_input_mouse = InputEventMouseButton.new()
		var new_input_pad = InputEventJoypadButton.new()
		
		InputMap.action_erase_events("ui_left")
		if node_data["ui_left_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_left"]))
			InputMap.action_add_event("ui_left", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_left_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_left"])
			InputMap.action_add_event("ui_left", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_left_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_left"])
			InputMap.action_add_event("ui_left", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()


		InputMap.action_erase_events("ui_right")
		if node_data["ui_right_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_right"]))
			InputMap.action_add_event("ui_right", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_right_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_right"])
			InputMap.action_add_event("ui_right", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_right_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_right"])
			InputMap.action_add_event("ui_right", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_up")
		if node_data["ui_up_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_up"]))
			InputMap.action_add_event("ui_up", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_up_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_up"])
			InputMap.action_add_event("ui_up", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_up_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_up"])
			InputMap.action_add_event("ui_up", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_down")
		if node_data["ui_down_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_down"]))
			InputMap.action_add_event("ui_down", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_down_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_down"])
			InputMap.action_add_event("ui_down", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_down_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_down"])
			InputMap.action_add_event("ui_down", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_shield")
		if node_data["ui_shield_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_shield"]))
			InputMap.action_add_event("ui_shield", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_shield_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_shield"])
			InputMap.action_add_event("ui_shield", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_shield_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_shield"])
			InputMap.action_add_event("ui_shield", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_left2")
		if node_data["ui_left2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_left2"]))
			InputMap.action_add_event("ui_left2", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_left2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_left2"])
			InputMap.action_add_event("ui_left2", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_left2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_left2"])
			InputMap.action_add_event("ui_left2", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_right2")
		if node_data["ui_right2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_right2"]))
			InputMap.action_add_event("ui_right2", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_right2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_right2"])
			InputMap.action_add_event("ui_right2", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_right2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_right2"])
			InputMap.action_add_event("ui_right2", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_up2")
		if node_data["ui_up2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_up2"]))
			InputMap.action_add_event("ui_up2", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_up2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_up2"])
			InputMap.action_add_event("ui_up2", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_up2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_up2"])
			InputMap.action_add_event("ui_up2", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_down2")
		if node_data["ui_down2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_down2"]))
			InputMap.action_add_event("ui_down2", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_down2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_down2"])
			InputMap.action_add_event("ui_down2", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_down2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_down2"])
			InputMap.action_add_event("ui_down2", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()

		InputMap.action_erase_events("ui_shield2")
		if node_data["ui_shield2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_shield2"]))
			InputMap.action_add_event("ui_shield2", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_shield2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_shield2"])
			InputMap.action_add_event("ui_shield2", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_shield2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_shield2"])
			InputMap.action_add_event("ui_shield2", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()

	save_option.close()
	
	
	
func _on_Back_pressed():
	save_option()
	get_tree().change_scene("res://scene/LoginMenu.tscn")
	pass # Replace with function body.
