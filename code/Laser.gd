extends Area2D

export var velocity = Vector2() 

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(true)
	connect("body_entered", self, "_on_area_enter")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	translate(velocity * delta)
	pass

func _on_area_enter(other):
	if other.is_in_group("player"):
		if other.perfect_shield:
			velocity.x = -velocity.x
			velocity.y = -velocity.y
			rotation_degrees = rotation_degrees + 180
		else:
			other.health -= 1
			velocity.x = 0
			velocity.y = 0
			$CollisionPolygon2D.disabled = true
			$CollisionPolygon2D.queue_free()
			$Sprite.visible = false
			$Sprite.queue_free()
			$MissileSmoke.visible = false
			$MissileSmoke.queue_free()
			$Explosion/AnimationPlayer.play("Explode")
			yield($Explosion/AnimationPlayer, "animation_finished")
			queue_free()
	pass
