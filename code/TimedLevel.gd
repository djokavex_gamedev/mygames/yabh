extends Node2D

var rand = RandomNumberGenerator.new()

#var new_level = preload("res://scene/TimedLevel.tscn")

var enemy_1 = preload("res://scene/Enemy.tscn")
var enemy_2 = preload("res://scene/SlowEnemy.tscn")

var global_timer_counter = 0
var min_enemy_speed = 150
var max_enemy_speed = 250

var MAX_VELOCITY = 500
var MIN_SPAWN_TIME = 0.05

# Called when the node enters the scene tree for the first time.
func _ready():
	match Global.level:
		1: #Meteorites
			remove_child($Blackhole)
			remove_child($Blackhole2)
			remove_child($Blackhole3)
			remove_child($BigLaserShip)
			remove_child($BigLaserShip2)
			remove_child($BigLaserShip3)
			remove_child($BigLaserShip4)
			remove_child($SatelliteSpawner)
			pass
		2:#Meteorites+Satellites
			remove_child($Blackhole)
			remove_child($Blackhole2)
			remove_child($Blackhole3)
			remove_child($BigLaserShip)
			remove_child($BigLaserShip2)
			remove_child($BigLaserShip3)
			remove_child($BigLaserShip4)
			#remove_child($SatelliteSpawner)
			pass
		3:#Meteorites+Satellites+Ship1
			remove_child($Blackhole)
			remove_child($Blackhole2)
			remove_child($Blackhole3)
			#remove_child($BigLaserShip)
			remove_child($BigLaserShip2)
			remove_child($BigLaserShip3)
			remove_child($BigLaserShip4)
			#remove_child($SatelliteSpawner)
			pass
		4:#Meteorites+Satellites+Ship1+Blackhole1
			#remove_child($Blackhole)
			remove_child($Blackhole2)
			remove_child($Blackhole3)
			#remove_child($BigLaserShip)
			remove_child($BigLaserShip2)
			remove_child($BigLaserShip3)
			remove_child($BigLaserShip4)
			#remove_child($SatelliteSpawner)
			pass
		5:#Meteorites+Satellites+Ship1+Blackhole123
			#remove_child($Blackhole)
			#remove_child($Blackhole2)
			#remove_child($Blackhole3)
			#remove_child($BigLaserShip)
			remove_child($BigLaserShip2)
			remove_child($BigLaserShip3)
			remove_child($BigLaserShip4)
			#remove_child($SatelliteSpawner)
			pass
		6:#Meteorites+Satellites+Ship1234
			remove_child($Blackhole)
			remove_child($Blackhole2)
			remove_child($Blackhole3)
			#remove_child($BigLaserShip)
			#remove_child($BigLaserShip2)
			#remove_child($BigLaserShip3)
			#remove_child($BigLaserShip4)
			#remove_child($SatelliteSpawner)
			pass
		7:#Meteorites+Satellites+Ship1234+Blackhole123
			#remove_child($Blackhole)
			#remove_child($Blackhole2)
			#remove_child($Blackhole3)
			#remove_child($BigLaserShip)
			#remove_child($BigLaserShip2)
			#remove_child($BigLaserShip3)
			#remove_child($BigLaserShip4)
			#remove_child($SatelliteSpawner)
			pass
		8:
			pass
		9:
			pass
		10:
			pass
		_:
			pass
			
	$Player.health = Global.player_lives
		
	$GuiLabel.text = "Level: " + str(Global.level) + " lives: " + str($Player.health)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$GuiLabel.text = "Level: " + str(Global.level) + " Timer: " + str(int($EndTimer.time_left)) + " lives: " + str($Player.health)
	pass


func _on_MeteorSpawner_timeout():
	var enemy = enemy_1.instance()
	#print("velocity" + str(enemy.velocity.x) + str(enemy.velocity.y)) 
	var screen_size = get_viewport().get_visible_rect().size
	
	rand.randomize()
	
	var x = 0
	var y = 0
	var side = rand.randi_range(0, 3)
	rand.randomize()
	if side == 0:
		x = 1
		y = rand.randf_range(1, screen_size.y-1)
		enemy.velocity.x = rand.randf_range(min_enemy_speed, max_enemy_speed)
		enemy.velocity.y = rand.randf_range(-20, 20)
	if side == 1:
		x = rand.randf_range(1, screen_size.x-1)
		y = 1
		enemy.velocity.x = rand.randf_range(-20, 20)
		enemy.velocity.y = rand.randf_range(min_enemy_speed, max_enemy_speed)
	if side == 2:
		x = screen_size.x - 1
		y = rand.randf_range(1, screen_size.y-1)
		enemy.velocity.x = -rand.randf_range(min_enemy_speed, max_enemy_speed)
		enemy.velocity.y = rand.randf_range(-20, 20)
	if side == 3:
		x = rand.randf_range(1, screen_size.x-1)
		y = screen_size.y -1
		enemy.velocity.x = rand.randf_range(-20, 20)
		enemy.velocity.y = -rand.randf_range(min_enemy_speed, max_enemy_speed)

	if enemy.velocity.x > MAX_VELOCITY:
		enemy.velocity.x = MAX_VELOCITY
	if enemy.velocity.x < -MAX_VELOCITY:
		enemy.velocity.x = -MAX_VELOCITY
	if enemy.velocity.y > MAX_VELOCITY:
		enemy.velocity.y = MAX_VELOCITY
	if enemy.velocity.y < -MAX_VELOCITY:
		enemy.velocity.y = -MAX_VELOCITY
	
	enemy.position.x = x
	enemy.position.y = y
	
	enemy.rotation_speed = rand.randf_range(-enemy.rotation_speed, enemy.rotation_speed)
	
	enemy.scale.x = rand.randf_range(0.5, 0.8)
	enemy.scale.y = enemy.scale.x
	
	add_child(enemy)

	global_timer_counter = global_timer_counter + 1
	
	if global_timer_counter > 30 :
		global_timer_counter = 0
			
		$MeteorSpawner.wait_time = $MeteorSpawner.wait_time * 0.9
		if $MeteorSpawner.wait_time < MIN_SPAWN_TIME:
			$MeteorSpawner.wait_time = MIN_SPAWN_TIME

		min_enemy_speed = min_enemy_speed * 1.1
		max_enemy_speed = max_enemy_speed * 1.1
	
	pass # Replace with function body.


func _on_SatelliteSpawner_timeout():
	var enemy = enemy_2.instance()
	#print("velocity" + str(enemy.velocity.x) + str(enemy.velocity.y)) 
	var screen_size = get_viewport().get_visible_rect().size
	
	rand.randomize()
	
	var x = 0
	var y = 0
	var side = rand.randi_range(0, 3)
	rand.randomize()
	if side == 0:
		x = 1
		y = rand.randf_range(1, screen_size.y-1)
		enemy.velocity.x = rand.randf_range(min_enemy_speed/2.0, max_enemy_speed/2.0)
		enemy.velocity.y = rand.randf_range(-20, 20)
	if side == 1:
		x = rand.randf_range(1, screen_size.x-1)
		y = 1
		enemy.velocity.x = rand.randf_range(-20, 20)
		enemy.velocity.y = rand.randf_range(min_enemy_speed/2.0, max_enemy_speed/2.0)
	if side == 2:
		x = screen_size.x - 1
		y = rand.randf_range(1, screen_size.y-1)
		enemy.velocity.x = -rand.randf_range(min_enemy_speed/2.0, max_enemy_speed/2.0)
		enemy.velocity.y = rand.randf_range(-20, 20)
	if side == 3:
		x = rand.randf_range(1, screen_size.x-1)
		y = screen_size.y -1
		enemy.velocity.x = rand.randf_range(-20, 20)
		enemy.velocity.y = -rand.randf_range(min_enemy_speed/2.0, max_enemy_speed/2.0)

	if enemy.velocity.x > MAX_VELOCITY/2.0:
		enemy.velocity.x = MAX_VELOCITY/2.0
	if enemy.velocity.x < -MAX_VELOCITY/2.0:
		enemy.velocity.x = -MAX_VELOCITY/2.0
	if enemy.velocity.y > MAX_VELOCITY/2.0:
		enemy.velocity.y = MAX_VELOCITY/2.0
	if enemy.velocity.y < -MAX_VELOCITY/2.0:
		enemy.velocity.y = -MAX_VELOCITY/2.0
	
	enemy.position.x = x
	enemy.position.y = y
	
	enemy.rotation_speed = rand.randf_range(-enemy.rotation_speed, enemy.rotation_speed)
	
	enemy.scale.x = rand.randf_range(0.8, 1.0)
	enemy.scale.y = enemy.scale.x
	
	add_child(enemy)

	global_timer_counter = global_timer_counter + 1
	
	if global_timer_counter > 5 :
		global_timer_counter = 0
			
		$SatelliteSpawner.wait_time = $SatelliteSpawner.wait_time * 0.9
		if $SatelliteSpawner.wait_time < MIN_SPAWN_TIME:
			$SatelliteSpawner.wait_time = MIN_SPAWN_TIME

		min_enemy_speed = min_enemy_speed * 1.1
		max_enemy_speed = max_enemy_speed * 1.1
	pass # Replace with function body.

func _on_EndTimer_timeout():
	print("You win")
	Global.level = Global.level + 1
	
	#var next_level = new_level.instance()
	#add_child(next_level)
	Global.player_lives = $Player.health
	
	$CanvasLayer/Pause/Label.text = "Congratulation! You complete level " + str(Global.level-1)
	$CanvasLayer/Pause.set_pause()
	
	#queue_free()
	pass # Replace with function body.
