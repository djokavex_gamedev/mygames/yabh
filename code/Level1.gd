extends Node2D

var rand = RandomNumberGenerator.new()
var enemy_1 = preload("res://scene/Enemy.tscn")
var enemy_2 = preload("res://scene/SlowEnemy.tscn")
var enemy_3 = preload("res://scene/BigLaserShip.tscn")
var player = preload("res://scene/Player.tscn")
var blackhole = preload("res://scene/Blackhole.tscn")

var global_timer_counter = 0
var min_enemy_speed = 150
var max_enemy_speed = 250

var MAX_VELOCITY = 500
var MIN_SPAWN_TIME = 0.05

# Called when the node enters the scene tree for the first time.
func _ready():
	
	match Global.difficulty:
		Global.difficulties.easy:
			Global.player_lives = 5
			Global.player_perfect_shield_time = 0.35
		Global.difficulties.normal:
			Global.player_lives = 3
			Global.player_perfect_shield_time = 0.30
		Global.difficulties.hard:
			Global.player_lives = 1
			Global.player_perfect_shield_time = 0.25
		_:
			Global.player_lives = 3
			Global.player_perfect_shield_time = 0.30
	Global.player_score = 0
	
	var music = AudioStreamPlayer.new()
	self.add_child(music)
	music.stream = load('res://sound/ObservingTheStar.ogg')
	music.play()
	
	var screen_size = get_viewport().get_visible_rect().size
	
	var pl = player.instance()
	pl.player_id = 1
	pl.position.x = screen_size.x / 2
	pl.position.y = screen_size.y / 2
	add_child(pl)
	
	
	var enemy = enemy_3.instance()
	
	rand.randomize()
	
	enemy.position.x = 10
	enemy.position.y = 50
	
	enemy.velocity.x = 10
	enemy.velocity.y = 0
	
	add_child(enemy)
	
	
	#var current_blackhole = blackhole.instance()
	
	#rand.randomize()
	
	#current_blackhole.position.x = screen_size.x / 2 + 100
	#current_blackhole.position.y = screen_size.y / 2 + 100
	
	#add_child(current_blackhole)
	
	
	
	pass
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_EnemySpawner_timeout():
	var enemy = enemy_1.instance()
	#print("velocity" + str(enemy.velocity.x) + str(enemy.velocity.y)) 
	var screen_size = get_viewport().get_visible_rect().size
	
	rand.randomize()
	
	var x = 0
	var y = 0
	var side = rand.randi_range(0, 3)
	rand.randomize()
	if side == 0:
		x = 1
		y = rand.randf_range(1, screen_size.y-1)
		enemy.velocity.x = rand.randf_range(min_enemy_speed, max_enemy_speed)
		enemy.velocity.y = rand.randf_range(-20, 20)
	if side == 1:
		x = rand.randf_range(1, screen_size.x-1)
		y = 1
		enemy.velocity.x = rand.randf_range(-20, 20)
		enemy.velocity.y = rand.randf_range(min_enemy_speed, max_enemy_speed)
	if side == 2:
		x = screen_size.x - 1
		y = rand.randf_range(1, screen_size.y-1)
		enemy.velocity.x = -rand.randf_range(min_enemy_speed, max_enemy_speed)
		enemy.velocity.y = rand.randf_range(-20, 20)
	if side == 3:
		x = rand.randf_range(1, screen_size.x-1)
		y = screen_size.y -1
		enemy.velocity.x = rand.randf_range(-20, 20)
		enemy.velocity.y = -rand.randf_range(min_enemy_speed, max_enemy_speed)

	if enemy.velocity.x > MAX_VELOCITY:
		enemy.velocity.x = MAX_VELOCITY
	if enemy.velocity.x < -MAX_VELOCITY:
		enemy.velocity.x = -MAX_VELOCITY
	if enemy.velocity.y > MAX_VELOCITY:
		enemy.velocity.y = MAX_VELOCITY
	if enemy.velocity.y < -MAX_VELOCITY:
		enemy.velocity.y = -MAX_VELOCITY
	
	enemy.position.x = x
	enemy.position.y = y
	
	enemy.rotation_speed = rand.randf_range(-enemy.rotation_speed, enemy.rotation_speed)
	
	enemy.scale.x = rand.randf_range(0.5, 0.8)
	enemy.scale.y = enemy.scale.x
	
	add_child(enemy)

	global_timer_counter = global_timer_counter + 1
	
	#print(str(global_timer_counter % 25)) 
	# Slowly Increase difficulty
	if (global_timer_counter % 25) == 0:
		
		var enemy2 = enemy_2.instance()
		#print("velocity" + str(enemy2.velocity.x) + str(enemy2.velocity.y)) 
		
		rand.randomize()
		
		x = 0
		y = 0
		side = rand.randi_range(0, 3)
		rand.randomize()
		if side == 0:
			x = 1
			y = rand.randf_range(1, screen_size.y-1)
			enemy2.velocity.x = 100#rand.randf_range(min_enemy_speed, max_enemy_speed)
			enemy2.velocity.y = rand.randf_range(-20, 20)
		if side == 1:
			x = rand.randf_range(1, screen_size.x-1)
			y = 1
			enemy2.velocity.x = rand.randf_range(-20, 20)
			enemy2.velocity.y = 100#rand.randf_range(min_enemy_speed, max_enemy_speed)
		if side == 2:
			x = screen_size.x - 1
			y = rand.randf_range(1, screen_size.y-1)
			enemy2.velocity.x = -100#rand.randf_range(min_enemy_speed, max_enemy_speed)
			enemy2.velocity.y = rand.randf_range(-20, 20)
		if side == 3:
			x = rand.randf_range(1, screen_size.x-1)
			y = screen_size.y -1
			enemy2.velocity.x = rand.randf_range(-20, 20)
			enemy2.velocity.y = -100#rand.randf_range(min_enemy_speed, max_enemy_speed)
	
		if enemy2.velocity.x > MAX_VELOCITY:
			enemy2.velocity.x = MAX_VELOCITY
		if enemy2.velocity.x < -MAX_VELOCITY:
			enemy2.velocity.x = -MAX_VELOCITY
		if enemy2.velocity.y > MAX_VELOCITY:
			enemy2.velocity.y = MAX_VELOCITY
		if enemy2.velocity.y < -MAX_VELOCITY:
			enemy2.velocity.y = -MAX_VELOCITY
		
		enemy2.position.x = x
		enemy2.position.y = y
		
		enemy2.rotation_speed = 1#rand.randf_range(-enemy2.rotation_speed, enemy2.rotation_speed)
		
		enemy2.scale.x = 1 #rand.randf_range(0.5, 0.8)
		enemy2.scale.y = enemy2.scale.x
		
		add_child(enemy2)
	
	if global_timer_counter > 50 :
		global_timer_counter = 0
			
		$EnemySpawner.wait_time = $EnemySpawner.wait_time * 0.9
		if $EnemySpawner.wait_time < MIN_SPAWN_TIME:
			$EnemySpawner.wait_time = MIN_SPAWN_TIME

		min_enemy_speed = min_enemy_speed * 1.1
		max_enemy_speed = max_enemy_speed * 1.1
		
	
	pass # Replace with function body.

