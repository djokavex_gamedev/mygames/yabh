extends Control



# Called when the node enters the scene tree for the first time.
func _ready():
	
	var save_option = File.new()
	if not save_option.file_exists("user://option.save"):
		return # Error! We don't have a save to load.

	# We need to revert the game state so we're not cloning objects
	# during loading. This will vary wildly depending on the needs of a
	# project, so take care with this step.
	# For our example, we will accomplish this by deleting saveable objects.
	#var save_nodes = get_tree().get_nodes_in_group("persistant_option")
	#for i in save_nodes:
	#	i.queue_free()

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_option.open("user://option.save", File.READ)
	while save_option.get_position() < save_option.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_option.get_line())

		var new_input_key = InputEventKey.new()
		var new_input_mouse = InputEventMouseButton.new()
		var new_input_pad = InputEventJoypadButton.new()
		
		InputMap.action_erase_events("ui_left")
		if node_data["ui_left_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_left"]))
			InputMap.action_add_event("ui_left", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_left_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_left"])
			InputMap.action_add_event("ui_left", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_left_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_left"])
			InputMap.action_add_event("ui_left", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()


		InputMap.action_erase_events("ui_right")
		if node_data["ui_right_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_right"]))
			InputMap.action_add_event("ui_right", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_right_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_right"])
			InputMap.action_add_event("ui_right", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_right_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_right"])
			InputMap.action_add_event("ui_right", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_up")
		if node_data["ui_up_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_up"]))
			InputMap.action_add_event("ui_up", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_up_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_up"])
			InputMap.action_add_event("ui_up", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_up_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_up"])
			InputMap.action_add_event("ui_up", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_down")
		if node_data["ui_down_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_down"]))
			InputMap.action_add_event("ui_down", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_down_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_down"])
			InputMap.action_add_event("ui_down", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_down_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_down"])
			InputMap.action_add_event("ui_down", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_shield")
		if node_data["ui_shield_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_shield"]))
			InputMap.action_add_event("ui_shield", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_shield_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_shield"])
			print("shield1 "+new_input_mouse.as_text())
			InputMap.action_add_event("ui_shield", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_shield_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_shield"])
			print("shield1 "+new_input_pad.as_text())
			InputMap.action_add_event("ui_shield", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_left2")
		if node_data["ui_left2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_left2"]))
			InputMap.action_add_event("ui_left2", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_left2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_left2"])
			InputMap.action_add_event("ui_left2", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_left2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_left2"])
			InputMap.action_add_event("ui_left2", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_right2")
		if node_data["ui_right2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_right2"]))
			InputMap.action_add_event("ui_right2", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_right2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_right2"])
			InputMap.action_add_event("ui_right2", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_right2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_right2"])
			InputMap.action_add_event("ui_right2", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_up2")
		if node_data["ui_up2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_up2"]))
			InputMap.action_add_event("ui_up2", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_up2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_up2"])
			InputMap.action_add_event("ui_up2", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_up2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_up2"])
			InputMap.action_add_event("ui_up2", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
		InputMap.action_erase_events("ui_down2")
		if node_data["ui_down2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_down2"]))
			InputMap.action_add_event("ui_down2", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_down2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_down2"])
			InputMap.action_add_event("ui_down2", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_down2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_down2"])
			InputMap.action_add_event("ui_down2", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()

		InputMap.action_erase_events("ui_shield2")
		if node_data["ui_shield2_type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(node_data["ui_shield2"]))
			InputMap.action_add_event("ui_shield2", new_input_key)
			new_input_key = InputEventKey.new()
		if node_data["ui_shield2_type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(node_data["ui_shield2"])
			print("shield2 "+new_input_mouse.as_text())
			InputMap.action_add_event("ui_shield2", new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if node_data["ui_shield2_type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(node_data["ui_shield2"])
			print("shield2 "+new_input_pad.as_text())
			InputMap.action_add_event("ui_shield2", new_input_pad)
			new_input_pad = InputEventJoypadButton.new()

	save_option.close()
	
	get_tree().connect("network_peer_connected", self, "_player_connected")
	
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Exit_pressed():
	get_tree().quit()
	pass # Replace with function body.


func _on_NewGame_pressed():
	Global.mode = Global.modes.score
	get_tree().change_scene("res://scene/ChooseDifficulty.tscn")
	pass # Replace with function body.


func _on_Versus_pressed():
	Global.player_lives = 5
	Global.player2_lives = 5
	Global.mode = Global.modes.versus
	get_tree().change_scene("res://scene/LevelVS.tscn")
	pass # Replace with function body.


func _on_Options_pressed():
	get_tree().change_scene("res://scene/InputRemapMenu.tscn")
	pass # Replace with function body.


func _on_ScoreBoard_pressed():
	get_tree().change_scene("res://scene/ScoreBoard.tscn")
	pass # Replace with function body.


func _on_Button_pressed():
	Global.player_lives = 5
	Global.mode = Global.modes.time
	Global.difficulty = Global.difficulties.easy
	get_tree().change_scene("res://scene/TimedLevel.tscn")
	pass # Replace with function body.



func _player_connected(id):
	print("Player connected to server!")
	#Game on!
	Global.network_player = id
	Global.player_lives = 5
	Global.mode = Global.modes.online_vs
	var game = preload("res://scene/OnlineVersus.tscn").instance()
	get_tree().get_root().add_child(game)
	hide()


func _on_HostMulti_pressed():
	print("Hosting network")
	var host = NetworkedMultiplayerENet.new()
	var res = host.create_server(4242,2)
	if res != OK:
		print("Error creating server")
		return
	$VBoxContainer/HBoxContainer/GuestMulti.hide()
	$VBoxContainer/HBoxContainer/HostMulti.disabled = true
	get_tree().set_network_peer(host)
	
	pass # Replace with function body.


func _on_GuestMulti_pressed():
	print("Joining network")
	var host = NetworkedMultiplayerENet.new()
	host.create_client("127.0.0.1",4242)
	get_tree().set_network_peer(host)
	$VBoxContainer/HBoxContainer/HostMulti.hide()
	$VBoxContainer/HBoxContainer/GuestMulti.disabled = true
	pass # Replace with function body.
