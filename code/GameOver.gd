extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	$VBoxContainer/score_label.text = "Score: " + str(Global.player_score)
	
	var score_array = []
	
	# List previous best scores
	var save_option = File.new()
	var node_data
	if save_option.file_exists("user://scoreboard"+str(Global.mode)+".save"):
		
		save_option.open("user://scoreboard"+str(Global.mode)+".save", File.READ)
		save_option.get_position()
		#while save_option.get_position() < save_option.get_len():
		# Get the saved dictionary from the next line in the save file
		node_data = parse_json(save_option.get_line())
		
		if Global.difficulty == Global.difficulties.easy:
			score_array.push_back(node_data["EASY_top1"])
			score_array.push_back(node_data["EASY_top2"])
			score_array.push_back(node_data["EASY_top3"])
			
		if Global.difficulty == Global.difficulties.normal:
			score_array.push_back(node_data["NORMAL_top1"])
			score_array.push_back(node_data["NORMAL_top2"])
			score_array.push_back(node_data["NORMAL_top3"])
			
		if Global.difficulty == Global.difficulties.hard:
			score_array.push_back(node_data["HARD_top1"])
			score_array.push_back(node_data["HARD_top2"])
			score_array.push_back(node_data["HARD_top3"])
			
		save_option.close()
	else:
		var save_score = File.new()
		save_score.open("user://scoreboard"+str(Global.mode)+".save", File.WRITE)
	
		var save_dict = {
			"mode": Global.mode,
			"EASY_top1" : 0,
			"EASY_top2" : 0,
			"EASY_top3" : 0,
			"NORMAL_top1" : 0,
			"NORMAL_top2" : 0,
			"NORMAL_top3" : 0,
			"HARD_top1" : 0,
			"HARD_top2" : 0,
			"HARD_top3" : 0
		}
		node_data = save_dict
		
		# Store the save dictionary as a new line in the save file.
		save_score.store_line(to_json(save_dict))
		save_score.close()
	
	if score_array.front() != null:
		$VBoxContainer/top1.text = str(score_array.pop_front())
	else:
		$VBoxContainer/top1.text = "0"
	if score_array.front() != null:
		$VBoxContainer/top2.text = str(score_array.pop_front())
	else:
		$VBoxContainer/top2.text = "0"
	if score_array.front() != null:
		$VBoxContainer/top3.text = str(score_array.pop_front())
	else:
		$VBoxContainer/top3.text = "0"
		
	if Global.player_score > $VBoxContainer/top1.text.to_int():
		$VBoxContainer/top3.text = $VBoxContainer/top2.text
		$VBoxContainer/top2.text = $VBoxContainer/top1.text
		$VBoxContainer/top1.text =  str(Global.player_score)
	else:
		if Global.player_score > $VBoxContainer/top2.text.to_int():
			$VBoxContainer/top3.text = $VBoxContainer/top2.text
			$VBoxContainer/top2.text = str(Global.player_score)
		else:
			if Global.player_score > $VBoxContainer/top3.text.to_int():
				$VBoxContainer/top3.text = str(Global.player_score)
	
	var save_score = File.new()
	save_score.open("user://scoreboard"+str(Global.mode)+".save", File.WRITE)
	var save_dict
	
	if Global.difficulty == Global.difficulties.easy:
		save_dict = {
			"mode": Global.mode,
			"EASY_top1" : $VBoxContainer/top1.text.to_int(),
			"EASY_top2" : $VBoxContainer/top2.text.to_int(),
			"EASY_top3" : $VBoxContainer/top3.text.to_int(),
			"NORMAL_top1" : node_data["NORMAL_top1"],
			"NORMAL_top2" : node_data["NORMAL_top2"],
			"NORMAL_top3" : node_data["NORMAL_top3"],
			"HARD_top1" : node_data["HARD_top1"],
			"HARD_top2" : node_data["HARD_top2"],
			"HARD_top3" : node_data["HARD_top3"]
		}
	if Global.difficulty == Global.difficulties.normal:
		save_dict = {
			"mode": Global.mode,
			"EASY_top1" : node_data["EASY_top1"],
			"EASY_top2" : node_data["EASY_top2"],
			"EASY_top3" : node_data["EASY_top3"],
			"NORMAL_top1" : $VBoxContainer/top1.text.to_int(),
			"NORMAL_top2" : $VBoxContainer/top2.text.to_int(),
			"NORMAL_top3" : $VBoxContainer/top3.text.to_int(),
			"HARD_top1" : node_data["HARD_top1"],
			"HARD_top2" : node_data["HARD_top2"],
			"HARD_top3" : node_data["HARD_top3"]
		}
	if Global.difficulty == Global.difficulties.hard:
		save_dict = {
			"mode": Global.mode,
			"EASY_top1" : node_data["EASY_top1"],
			"EASY_top2" : node_data["EASY_top2"],
			"EASY_top3" : node_data["EASY_top3"],
			"NORMAL_top1" : node_data["NORMAL_top1"],
			"NORMAL_top2" : node_data["NORMAL_top2"],
			"NORMAL_top3" : node_data["NORMAL_top3"],
			"HARD_top1" : $VBoxContainer/top1.text.to_int(),
			"HARD_top2" : $VBoxContainer/top2.text.to_int(),
			"HARD_top3" : $VBoxContainer/top3.text.to_int()
		}

	# Store the save dictionary as a new line in the save file.
	save_score.store_line(to_json(save_dict))
	save_score.close()
	
	if Global.mode == Global.modes.versus:
		if Global.looser == 1:
			$VBoxContainer/winner.text = "Player2 WIN"
		if Global.looser == 2:
			$VBoxContainer/winner.text = "Player1 WIN"
	else:
		$VBoxContainer/winner.text = ""
		
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_retry_button_pressed():
	match Global.mode:
		Global.modes.score:
			get_tree().change_scene("res://scene/Level1.tscn")
		Global.modes.versus:
			get_tree().change_scene("res://scene/LevelVS.tscn")
		Global.modes.time:
			Global.player_lives = 5
			get_tree().change_scene("res://scene/TimedLevel.tscn")
		_:
			get_tree().change_scene("res://scene/Level1.tscn")
	pass # Replace with function body.


func _on_main_menu_pressed():
	get_tree().change_scene("res://scene/LoginMenu.tscn")
	pass # Replace with function body.


