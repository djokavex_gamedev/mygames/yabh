extends Area2D
#extends KinematicBody2D

export var velocity = Vector2() 
export var rotation_speed = 1.0

signal died

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(true)
	add_to_group("enemy")
	var text_gui = get_tree().get_root().find_node("level_gui",true,false)
	self.connect("died",text_gui,"enemy_died")
	connect("body_entered", self, "_on_area_enter")
	pass # Replace with function body.

func _process(delta):
	rotation = rotation + rotation_speed * delta
	translate(velocity * delta)
	
	if position.x >= get_viewport_rect().size.x:
		emit_signal('died')
		queue_free()
	if position.x <= 0:
		emit_signal('died')
		queue_free()
	if position.y >= get_viewport_rect().size.y:
		emit_signal('died')
		queue_free()
	if position.y <= 0:
		emit_signal('died')
		queue_free()
	pass

func _on_area_enter(other):
	if other.is_in_group("player"):
		other.health -= 1
		$CollisionPolygon2D.disabled = true
		$CollisionPolygon2D.queue_free()
		$Sprite.visible = false
		$Sprite.queue_free()
		$Explosion/AnimationPlayer.play("Explode")
		yield($Explosion/AnimationPlayer, "animation_finished")
		queue_free()
	pass
