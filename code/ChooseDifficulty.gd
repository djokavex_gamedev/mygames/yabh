extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_easy_pressed():
	Global.difficulty = Global.difficulties.easy
	get_tree().change_scene("res://scene/Level1.tscn")
	pass # Replace with function body.


func _on_normal_pressed():
	Global.difficulty = Global.difficulties.normal
	get_tree().change_scene("res://scene/Level1.tscn")
	pass # Replace with function body.


func _on_hard_pressed():
	Global.difficulty = Global.difficulties.hard
	get_tree().change_scene("res://scene/Level1.tscn")
	pass # Replace with function body.
