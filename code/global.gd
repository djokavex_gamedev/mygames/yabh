extends Node

enum difficulties{
  easy,
  normal,
  hard
}

var difficulty = difficulties.normal

enum modes{
  score,
  versus,
  online_vs,
  time,
  letter_solo,
  letter_coop
}

var mode = modes.score

enum powerdown{
  bigger,
  slower,
  invisible
}

var power_down_bigger_size = 0.25
var power_down_bigger_time = 2.0
var power_down_slower_value = 20
var power_down_slower_time = 2.0
var power_down_invisible_time = 2.0


var player_lives = 1
var player_shield = 2
var player_score = 0
var player2_lives = 1
var player2_shield = 2
var player2_score = 0

var looser = 1

var player_perfect_shield_time = 0.3

var level = 1

var network_player = -1


