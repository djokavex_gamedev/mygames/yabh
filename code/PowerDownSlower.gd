extends Area2D

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("power_down")
	connect("body_entered", self, "_on_area_enter")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_area_enter(other):
	if other.is_in_group("player"):
		other.take_power_down(Global.powerdown.slower)
		queue_free()
	pass
