extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	
	# List previous best scores
	var save_option = File.new()
	var node_data
	if save_option.file_exists("user://scoreboard"+str(Global.modes.score)+".save"):
		
		save_option.open("user://scoreboard"+str(Global.modes.score)+".save", File.READ)
		save_option.get_position()
		#while save_option.get_position() < save_option.get_len():
		# Get the saved dictionary from the next line in the save file
		node_data = parse_json(save_option.get_line())
		
		$VBoxContainer/HBoxContainer/VBoxContainer/VBoxContainer3/top1.text = str(node_data["EASY_top1"])
		$VBoxContainer/HBoxContainer/VBoxContainer/VBoxContainer3/top2.text = str(node_data["EASY_top2"])
		$VBoxContainer/HBoxContainer/VBoxContainer/VBoxContainer3/top3.text = str(node_data["EASY_top3"])
		
		$VBoxContainer/HBoxContainer/VBoxContainer/VBoxContainer2/top1.text = str(node_data["NORMAL_top1"])
		$VBoxContainer/HBoxContainer/VBoxContainer/VBoxContainer2/top2.text = str(node_data["NORMAL_top2"])
		$VBoxContainer/HBoxContainer/VBoxContainer/VBoxContainer2/top3.text = str(node_data["NORMAL_top3"])
		
		$VBoxContainer/HBoxContainer/VBoxContainer/VBoxContainer/top1.text = str(node_data["HARD_top1"])
		$VBoxContainer/HBoxContainer/VBoxContainer/VBoxContainer/top2.text = str(node_data["HARD_top2"])
		$VBoxContainer/HBoxContainer/VBoxContainer/VBoxContainer/top3.text = str(node_data["HARD_top3"])
			
		save_option.close()
	else:
		var save_score = File.new()
		save_score.open("user://scoreboard"+str(Global.modes.score)+".save", File.WRITE)
	
		var save_dict = {
			"mode": Global.mode,
			"EASY_top1" : 0,
			"EASY_top2" : 0,
			"EASY_top3" : 0,
			"NORMAL_top1" : 0,
			"NORMAL_top2" : 0,
			"NORMAL_top3" : 0,
			"HARD_top1" : 0,
			"HARD_top2" : 0,
			"HARD_top3" : 0
		}
	
		# Store the save dictionary as a new line in the save file.
		save_score.store_line(to_json(save_dict))
		save_score.close()
		
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	get_tree().change_scene("res://scene/LoginMenu.tscn")
	pass # Replace with function body.
