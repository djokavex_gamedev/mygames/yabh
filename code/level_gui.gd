extends Label

# Called when the node enters the scene tree for the first time.
func _ready():
	set_text("Score: " + str(Global.player_score))
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	set_text("Score: " + str(Global.player_score))
	pass


func _on_Player_health_changed(player_health):
	#set_text("Life: " + str(player_health) + " / Score: " + str(Global.player_score))
	Global.player_lives = player_health
	pass # Replace with function body.

func enemy_died():
	Global.player_score = Global.player_score + 1
	#set_text("Life: " + str(current_health) + " / Score: " + str(Global.player_score))
	pass
